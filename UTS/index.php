<?php 

session_start();
if(!isset($_SESSION['data'])){
    $_SESSION['data'] = array();
}

if(isset($_POST['submit'])){
    array_push($_SESSION['data'], array($_POST['Nomer_Resi'],
        $_POST['Ekspedisi'],
        $_POST['Barang'],
        $_POST['Harga_ongkir'])
    );
}

if(isset($_POST['reset'])){
    session_unset();
    session_destroy();
}
// print_r($_SESSION['data']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>UTS Zaki</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <!-- <li class="nav-item">
                <a class="nav-link" href="#">info</a>
                </li> -->
                <li class="nav-item">
                <a class="nav-link disabled">Disabled</a>
                </li>
            </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <br>
        <div class="box">
            <form action="" method="post">
                Nomer Resi
                <input name="Nomer Resi" class="form-control" type="text"  aria-label="default input example">
                Ekspedisi
                <input name="Ekspedisi" class="form-control" type="text" aria-label="default input example">
                Barang
                <input name="Barang" class="form-control" type="text" aria-label="default input example">
                Harga ongkir
                <input name="Harga ongkir" class="form-control" type="text" aria-label="default input example">
                <br>
                <div class="d-grid gap-2">
                    <button name="submit" class="btn btn-primary" type="submit" value="submit">Simpan</button>
                </div>
            </form>
        </div>
        <br><br><br>
        <h1>TABEL PENGIRIMAN</h1>
        
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nomer Resi</th>
                    <th>Ekspedisi</th>
                    <th>Barang</th>
                    <th>Harga ongkir</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if(isset($_SESSION['data'])){$i=1; foreach($_SESSION['data'] as $row){?>
                    <tr>
                        <td><?=$i?></td>
                        <td><?=$row[0]?></td>
                        <td><?=$row[1]?></td>
                        <td><?=$row[2]?></td>
                        <td><?=$row[3]?></td>
                    </tr>
                <?php $i++;}}?>
            </tbody>
        </table>
        <form action="" method="post">
            <button name="reset" type="submit" class="btn btn-danger">Reset Tabel</button>
        </form>

    <br><br><br>
</body>
</html>